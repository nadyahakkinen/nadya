const colorTheme = {
  transitionValue: "var(--default-transition)", 
  elements: {
    themeOptionLight: document.getElementById('light-theme'),
    themeOptionDark: document.getElementById('dark-theme'),
    body: document.querySelector('body'),
  }
};

(function () {
  const setTheme = () => {
    const storedTheme = localStorage.getItem('color-theme');
    
    if (storedTheme === 'light') {
      document.documentElement.setAttribute('data-color-theme', storedTheme);
      colorTheme.elements.themeOptionLight.checked = true;
    } else {
      document.documentElement.removeAttribute('data-color-theme');
      colorTheme.elements.themeOptionDark.checked = true;
    }
  };

  window.addEventListener('DOMContentLoaded', () => {
    setTheme();

    document.querySelectorAll('[data-color-theme-value]').forEach(input => {
      input.addEventListener('click', () => {
        const selectedTheme = input.getAttribute('data-color-theme-value');

        if (selectedTheme === 'light') {
          localStorage.setItem('color-theme', 'light')
        } else {
          localStorage.removeItem('color-theme');
        }

        setTheme();
      });
    });

    setTheme();
  });
})();
