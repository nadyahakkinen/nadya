const blurredPreview = document.querySelectorAll('.blurred-preview');

blurredPreview.forEach(div => {
  const fullImg = div.querySelector('img')

  function loaded() {
    div.classList.add("loaded")
  }

  if (fullImg.complete) {
    loaded()
  } else {
    fullImg.addEventListener("load", loaded)
  }
})

console.log(blurredPreview);
