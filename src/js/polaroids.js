const polaroids = document.querySelectorAll('.card--polaroid');

const observer = new IntersectionObserver(
  entries => {
    entries.forEach(entry => {
      entry.target.classList.toggle('fade-in', entry.isIntersecting)
      if (entry.isIntersecting) observer.unobserve(entry.target)
    })
  },
  {
    threshold: 0.5
  }
)

polaroids.forEach(polaroid => {
  observer.observe(polaroid)
})
